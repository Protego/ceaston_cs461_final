﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS461Final
{
    interface EventListener
    {
        void ProcessEvent();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS461Final
{
    class BaseController : EventListener
    {
        
        public BaseController()
        {
        }
        /// <summary>
        /// Here is a better or maybe not better summary
        /// </summary>
        public void ProcessEvent()
        {
            
        }
        public View View { get; set; }
        public Model Model { get; set; }

    }
}
